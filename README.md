Celem projektu jest zbadanie problemu równoległego rozwiązywania układów równań liniowych.
W jego ramach powstaną dwie implementacje: sekwencyjna i równoległa, z wykorzystaniem
technologii CUDA. Szczególna uwaga zostanie poświęcona porównaniu rezultatów wymienionych
rozwiązań.
