#include <iostream>
#include <fstream>
#include <cmath>

#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include "helper.cuh"


using namespace std;

inline void cudaErr( string err, cudaError_t status){
	cout<< err << cudaGetErrorString(status)<<endl;
	exit(-1);
}

__global__ void gauss(float* mat, int n, int i){	// kernele maj¹ zawsze identyfikator __global__ i musz¹ zwracaæ void
	int j= threadIdx.x;

    int k;
    float m;
    int row_len = n + 1;

    if(j > i && j < n){
        m = -mat[j*row_len+i] / mat[i*row_len+i];
        for(k = i + 1; k <= n; k++)
            mat[j*row_len+k] += m * mat[i*row_len+k];
    }
}


void seqGauss(int n, float * mat, float * result){

    int i,j,k;
    float m;
    int row_len = n+1;

    for(i = 0; i < n - 1; i++){
        for(j = i + 1; j < n; j++){
            m = -mat[j*row_len+i] / mat[i*row_len+i];
            for(k = i + 1; k <= n; k++)
                mat[j*row_len+k] += m * mat[i*row_len+k];
        }
    }
}

void getResult(int n, float* mat, float* result){

    int i,j;
    float s;
    int row_len = n+1;

    for(i = n - 1; i >= 0; i--){
        s = mat[i*(n+1)+n];
        for(j = n - 1; j >= i + 1; j--)
            s -= mat[i*row_len+j] * result[j];

        result[i] = s / mat[i*row_len+i];
    }
}


void mat_init(float* mat, int n){
    for(int i = 0; i < n; i++){
        for(int j = 0; j < n+1; j++)
            mat[i*(n+1)+j] = 0.0;
    }
}

void mat_delete(float* mat, int n){
     delete[] mat;
}

void mat_read(FILE* file, float* mat, int n){
    for(int i = 0; i < n; i++)
        for(int j = 0; j < n+1; j++)
            fscanf(file,"%f",&mat[i*(n+1)+j]);

}

void mat_print(float* mat, int n){
    printf("n = %d\n", n);
    for(int i = 0; i < n; i++){
        for(int j = 0; j < n+1; j++)
            printf("%8.4f ", mat[i*(n+1)+j]);

        printf("\n");
    }
    printf("\n");
}

void vect_init(float* v, int n){
    for(int i = 0; i < n; i++)
        v[i] = 0.0;
}

void vect_print(float* v, int n){
    printf("[ ");
    for(int i = 0; i < n; i++)
        printf("%6.4f ", v[i]);
    
    printf("]\n");
}

void resCopy(float* mat, float* res,int n){
    for(int i = 0; i < n; i++)
        res[i] = mat[i*(n+1)+n];
}


int main(int argc, char** argv){

    if(argc < 2){
        printf("Sorry bro, input matrix needed\n");
        return 0;
    }
    

    // init
    FILE* file;
    float* matA;
    float* matB;
    int n;
    float* result;
    float* cudaResult;
    
    // CUDA stuff
    float* d_matB;
	cudaError_t status;		// funkcje cuda zwracaja kod bledu jako strukture cudaError_t

    file = fopen(argv[1],"r");
    fscanf(file,"%d",&n);
    matA = new float[n*(n+1)];
    mat_init(matA, n);
    mat_read(file, matA, n);
    fclose(file);

    file = fopen(argv[1],"r");
    fscanf(file,"%d",&n);
    matB = new float[n*(n+1)];
    mat_init(matB, n);
    mat_read(file, matB, n);
    fclose(file);

    printf("Input matrix:\n");
    mat_print(matA, n);

    result = new float[n];
    cudaResult = new float[n];

    vect_init(result,n);
    vect_init(cudaResult,n);

    printf("************ SECQUENCE STYLE ************\n");
    seqGauss(n,matA,result);
    mat_print(matA, n);
    getResult(n,matA,result);
    vect_print(result,n);

    
    //** CUDA Panie, CUDA **//
    /***********************/
    
	// alokacja pamieci na karcie
	status= cudaMalloc( (void**) &(d_matB), sizeof(float)*n*(n+1));
	if( status != cudaSuccess)
		cudaErr("Error: cudaMalloc for d_matB: ", status);

	// kopiowanie z hosta na karte
	status= cudaMemcpy( d_matB, matB, sizeof(float)*n*(n+1), cudaMemcpyHostToDevice);
	if( status != cudaSuccess )
		cudaErr("Error: memCpy Host->Device: ", status);

    // obliczenia na karcie
    printf("/************ CUDA STYLE ************\n");
    for(int i = 0; i < n - 1; i++)
        gauss<<< 1, n>>>(d_matB, n, i);

    // kopiowanie z karty na hosta 
	status= cudaMemcpy(matB, d_matB, sizeof(float) * n * (n+1), cudaMemcpyDeviceToHost);
	if( status != cudaSuccess)
		cudaErr("Error: memCpy Device -> Host: ", status);

    mat_print(matB,n);
    getResult(n,matB,cudaResult);
    vect_print(cudaResult,n);

    //** Koniec CUDA Panie, koniec **//
    /********************************/

    // clean up
    mat_delete(matA, n);
    mat_delete(matB, n);
    delete[] result;
    delete[] cudaResult;
    cudaFree(d_matB);

    return 0;
}
