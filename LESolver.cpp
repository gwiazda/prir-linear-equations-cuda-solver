#include "LESolver.hpp"

/* Gauss */
void seqGauss(int n, float * mat, float * result){

    int i,j,k;
    float m;
    int row_len = n+1;

    for(i = 0; i < n - 1; i++){
        for(j = i + 1; j < n; j++){
            m = -mat[j*row_len+i] / mat[i*row_len+i];
            for(k = i + 1; k <= n; k++)
                mat[j*row_len+k] += m * mat[i*row_len+k];
        }
    }
}

void getResult(int n, float* mat, float* result){

    int i,j;
    float s;
    int row_len = n+1;

    for(i = n - 1; i >= 0; i--){
        s = mat[i*(n+1)+n];
        for(j = n - 1; j >= i + 1; j--)
            s -= mat[i*row_len+j] * result[j];

        result[i] = s / mat[i*row_len+i];
    }
}

/* Gauss- Jordan */
template<typename T>
void lseqGJ(T **A, T *B, T *x, int n){
	for(int k= 0; k < n; ++k){
		for(int i= 0 ; i < n; ++i){
			if(i== k) continue;
			T alfa= A[i][k]/A[k][k];
			for(int j= k; j < n; ++j){
				A[i][j]-=  alfa * A[k][j] ;
			}
			B[i] -=  alfa * B[k];
		}
	}

	for(int k= 0; k < n; ++k)
		x[k]= B[k] / A[k][k];
};

//template<typename T>
void seqGJ(float *A, float *B, float *x, int n){
	for(int k= 0; k < n; ++k){
		for(int i= 0 ; i < n; ++i){
			if(i== k) continue;
			float alfa= A[i*n+k]/A[k*n+k];
			for(int j= k; j < n; ++j)
				A[i*n+j]-=  alfa * A[k*n+j] ;
			B[i] -=  alfa * B[k];
		}
	}
	for(int k= 0; k < n; ++k)
		x[k]= B[k] / A[k*n+k];
}