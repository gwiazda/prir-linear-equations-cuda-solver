#ifndef CULESOLVER_CUH
#define CULESOLVER_CUH

#include "helper.cuh"

#define th 256 

/* eliminacja gaussa */
__global__ void gauss(float* mat, int n, int i);

/* eliminacja gaussa jordana */
template<typename T>
__global__ void cuGetResult(T *A, T *b, T *x, const int n);
template<typename T>
__global__ void cuGJ(T *A, T *b, const int n, const int k);
template<typename T>
T* GJelimination(T *a, T *b, int n);

/*** DEFINICJE ***/
/* G */
__global__ void gauss(float* mat, int n, int i){	// kernele maj¹ zawsze identyfikator __global__ i musz¹ zwracaæ void
	//int j= threadIdx.x;
	int j=  blockIdx.x * blockDim.x + threadIdx.x;		

    int k;
    float m;
    int row_len = n + 1;

    if(j > i && j < n){
        m = -mat[j*row_len+i] / mat[i*row_len+i];
        for(k = i + 1; k <= n; k++)
            mat[j*row_len+k] += m * mat[i*row_len+k];
    }
}
/* GJ */
template<typename T>
__global__ void cuGJ(T *A, T *b, const int n, const int k){
	const int i=  blockIdx.x * blockDim.x + threadIdx.x;		

	if(i == k || i >= n)	goto ret;
	T alfa= A[i*n + k] / A[k*n + k];
	for(int j= k; j < n; ++j)
		A[i*n + j]-= alfa * A[k*n +j];
	b[i]-= alfa * b[k];

ret:
	__syncthreads();
}

template<typename T>
__global__ void cuGetResult(T *A, T *b, const int n){
	const int idx=  blockIdx.x * blockDim.x + threadIdx.x;
	if( idx < n){
		b[idx]= b[idx] / A[idx*n + idx];
	}
	__syncthreads();
}

template<typename T>
T* GJelimination(T *a, T *b, int n){
	T *x= new T[n];
	T *da, *db;
	int blocks, threads= th;
	blocks= n/ threads;
	if( (n % threads) != 0)
		blocks++;
	cMalloc<T>(&da, n*n);
	cMalloc<T>(&db, n);

	sendVect<T>(da, a, n*n);
	sendVect<T>(db, b, n);

	for(int k= 0; k < n; ++k)
		cuGJ<float><<< blocks, threads >>>(da, db, n, k);

	cuGetResult<float><<< blocks, threads >>>(da, db, n);
	//cudaDeviceSynchronize();
	reciveVect<T>(x, db, n);

	cudaFree(da);
	cudaFree(db);

	return x;
}


#endif
