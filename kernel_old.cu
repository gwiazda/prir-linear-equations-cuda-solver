#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include "helper.cuh"
#include "gj.cuh"
#include <stdio.h>
#include <iostream>

using namespace std;

void wyp(float* a, float*b){
	a[0]= 1; a[1]= 1;
	a[2]=2; a[3]= 3;
	b[0]= 5; b[1]= 13; //b[2]= 9;
}
void ggjj(float **A, float *B, float *x, int n){
	for(int k= 0; k < n; ++k){
		for(int i= 0 ; i < n; ++i){
			if(i== k) continue;
			float alfa= A[i][k]/A[k][k];
			for(int j= k; j < n; ++j){
				A[i][j]-=  alfa * A[k][j] ;
			}
			B[i] -=  alfa * B[k];
		}
	}

	for(int k= 0; k < n; ++k)
		x[k]= B[k] / A[k][k];
}
void ggjj(float *A, float *B, float *x, int n){
	for(int k= 0; k < n; ++k){
		for(int i= 0 ; i < n; ++i){
			if(i== k) continue;
			float alfa= A[i*n+k]/A[k*n+k];
			for(int j= k; j < n; ++j){
				A[i*n+j]-=  alfa * A[k*n+j] ;
			}
			B[i] -=  alfa * B[k];
		}
	}

	for(int k= 0; k < n; ++k)
		x[k]= B[k] / A[k*n+k];
}

int main(int argc, char** argv){

	float *a, *b, *x;
	const int n= 2;

	a= new float[n*n];
	b= new float[n];
	x= new float[n];

	wyp(a,b);

	/*GJ<<< 1, n >>>(da, db, dx, n);
	cudaDeviceSynchronize();*/

	x= GJelimination(a, b, n);

	cout<<"x: ";
	for(int i= 0; i < n; ++i)
		cout<<x[i]<<", ";
	cout<<endl;
	/*cout<<"b: ";
	for(int i= 0; i < n; ++i)
		cout<<b[i]<<", ";
	cout<<endl;
	cout<<"a: ";
	for(int i= 0; i < n*n; ++i)
		cout<<a[i]<<", ";
	cout<<endl;*/

	wyp(a,b);

	float **A;
	A= new float*[3];
	A[0]= new float[3];
	A[1]= new float[3];
	A[2]= new float[3];
	A[0][0]= 3; A[0][1]= 4; A[0][2]= 1;
	A[1][0]= 1; A[1][1]= 1; A[1][2]= 1;
	A[2][0]= 2; A[2][1]= -1; A[2][2]= 4;
	ggjj(a, b, x, n);

	for(int i= 0; i < n; ++i)
		cout<<x[i]<<", ";
	cout<<endl;
	for(int i= 0; i < n; ++i)
		cout<<b[i]<<", ";
	cout<<endl;
	for(int i= 0; i < n*n; ++i)
		cout<<a[i]<<", ";
	cout<<endl;

	system("PAUSE");
	return 0;
}