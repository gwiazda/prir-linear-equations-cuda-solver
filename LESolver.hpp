/*****************************************************/
/* Sekwencyjne funkcje rozwiązujące rownania liniowe */
/* metodami eliminacji Gaussa i Gaussa- Jordana		 */
/*****************************************************/
#ifndef LESOLVER_HPP
#define LESOLVER_HPP

/* Gauss */
void seqGauss(int n, float * mat, float * result);
void getResult(int n, float* mat, float* result);

/* Gauss- Jordan */
template<typename T>
void lseqGJ(T **A, T *B, T *x, int n);

//template<typename T>
void seqGJ(float *A, float *B, float *x, int n);
#endif