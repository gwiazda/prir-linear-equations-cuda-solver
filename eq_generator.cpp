#include <stdio.h>
#include <stdlib.h>
int main(int argc, char **argv){

    if(argc < 3){
        printf("Sorry bro, 2 arguments needed:\n./eq_generator output_file_name n\n");
        return 0;
    }

    int n;
    FILE* file;
    double s;

    file = fopen(argv[1],"w");
    n = atoi(argv[2]);

    fprintf(file,"%d\n",n);

    for(int i=0; i < n; i++){
        s = 0.0;
        for(int j=0; j < n; j++){
            double aij= (double)rand() / RAND_MAX;
            s+= aij * j;
            fprintf(file,"%1.20lf ", aij);
        }
        fprintf(file,"%lf\n", s);
    }

    return 0;
}
