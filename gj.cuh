#ifndef GJ_CUH
#define GJ_CUH
#include "cuda_runtime.h"
#include "helper.cuh"

template<typename T>
__global__ void cuGetResult(T *A, T *b, T *x, const int n);
template<typename T>
__global__ void cuGJ(T *A, T *b, const int n, const int k);

template<typename T>
T* GJelimination(T *a, T *b, int n);

/*** DEFINICJE ***/
template<typename T>
__global__ void cuGJ(T *A, T *b, const int n, const int k){
	const int i=  blockIdx.x * blockDim.x + threadIdx.x;		

	if(i == k || i >= n)	goto ret;
	T alfa= A[i*n + k] / A[k*n + k];
	for(int j= k; j < n; ++j)
		A[i*n + j]-= alfa * A[k*n +j];
	b[i]-= alfa * b[k];

ret:
	__syncthreads();
}

template<typename T>
__global__ void cuGetResult(T *A, T *b, const int n){
	const int idx=  blockIdx.x * blockDim.x + threadIdx.x;
	if( idx < n){
		b[idx]= b[idx] / A[idx*n + idx];
	}
	__syncthreads();
}

template<typename T>
T* GJelimination(T *a, T *b, int n){
	T *x= new T[n];
	T *da, *db;

	cMalloc<T>(&da, n*n);
	cMalloc<T>(&db, n);
	//cMalloc<T>(&dx, n);

	sendVect<T>(da, a, n*n);
	sendVect<T>(db, b, n);

	for(int k= 0; k < n; ++k)
		cuGJ<float><<< 1, n >>>(da, db, n, k);

	cuGetResult<float><<< 1, n >>>(da, db, n);
	//cudaDeviceSynchronize();

	reciveVect<T>(x, db, n);

	cudaFree(da);
	cudaFree(db);
	//cudaFree(dx);

	return x;
}
#endif