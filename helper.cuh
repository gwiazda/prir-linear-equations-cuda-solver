/* Z g�ry przepraszam, �e cia�a metod znajduj� si� w pliku *.cuh.
	Tak by� nei powinno, ale CUDA wymaga, �eby metody znajdowa�y si� w jednej jednostce kompilacyjnej.
	*/
#ifndef HELPER_CUH
#define HELPER_CUH

#include "cuda_runtime.h"
#include <iostream>

// cudaMemcpy Host->Device
template<typename T>
void sendVect(T* dest, T* src, int size);

//cudaMemcpy Device -> Host
template<typename T>
void reciveVect(T* dest, T* src, int size);

//cudaMalloc
template<typename T>
void cMalloc(T** vect, int size);

inline void statCheck(cudaError_t status);

/**** DEFINICJE ****/
// cudaMemcpy Host->Device
template<typename T>
void sendVect(T* dest, T* src, int size){	
	cudaError_t status= cudaMemcpy( dest, src, size*sizeof(T), cudaMemcpyHostToDevice);
	statCheck(status);
}

//cudaMemcpy Device -> Host
template<typename T>
void reciveVect(T* dest, T* src, int size){
	cudaError_t status= cudaMemcpy( dest, src, size*sizeof(T), cudaMemcpyDeviceToHost);
	statCheck(status);
}

//cudaMalloc
template<typename T>
void cMalloc(T** vect, int size){
	cudaError_t status= cudaMalloc( (void**)vect, size*sizeof(T));
	statCheck(status);
}

///////////////
inline void statCheck(cudaError_t status){
	if( status != cudaSuccess){
		std::cerr<<cudaGetErrorString(status)<<std::endl;
		abort();
	}
}

#endif