CC=nvcc
CFLAGS= -c -o
OBJS=gauss
EXE= kernel

all: solver eq_generator gen run 

solver: kernel.cu LESolver.o
		$(CC) kernel.cu LESolver.o -o $(EXE)

gauss: $(OBJS)
		$(CC) $^ -o $@ 

lessolver: LESolver.cpp
		$(CC) $(CFLAGS) LESolver.o LESolver.cpp 
		
eq_generator: eq_generator.cpp
	g++ eq_generator.cpp -o eq_generator

run:
	./$(EXE) mTestGen10

gen:
	./eq_generator mTestGen10 10

clean:
	rm -rf gauss eq_generator mTestGen* $(EXE) LESolver.o

