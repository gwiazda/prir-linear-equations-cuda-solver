#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include <stdio.h>
#include <iostream>
#include <time.h>

#include "cuLESolver.cuh"
#include "LESolver.hpp"

using namespace std;

inline void cudaErr( string err, cudaError_t status){
	cout<< err << cudaGetErrorString(status)<<endl;
	exit(-1);
}
void mat_init(float* mat, int n){
    for(int i = 0; i < n; i++){
        for(int j = 0; j < n+1; j++)
            mat[i*(n+1)+j] = 0.0;
    }
}

void mat_delete(float* mat, int n){
     delete[] mat;
}

void mat_read(FILE* file, float* mat, int n){
    for(int i = 0; i < n; i++)
        for(int j = 0; j < n+1; j++)
            fscanf(file,"%f",&mat[i*(n+1)+j]);

}

void mat_print(float* mat, int n){
    printf("n = %d\n", n);
    for(int i = 0; i < n; i++){
        for(int j = 0; j < n+1; j++)
            printf("%8.4f ", mat[i*(n+1)+j]);

        printf("\n");
    }
    printf("\n");
}

void vect_init(float* v, int n){
    for(int i = 0; i < n; i++)
        v[i] = 0.0;
}

void vect_print(float* v, int n){
    printf("[ ");
    for(int i = 0; i < n; i++)
        printf("%6.4f ", v[i]);
    
    printf("]\n");
}

void resCopy(float* mat, float* res,int n){
    for(int i = 0; i < n; i++)
        res[i] = mat[i*(n+1)+n];
}


int main(int argc, char** argv){

    if(argc < 2){
        printf("Sorry bro, input matrix needed\n");
        return 0;
    }
    // init
    FILE* file;
    float* matA;
    float* matB;
    int n;
    float* result;
    float* cudaResult;

    // time vars
    clock_t start, end; 
    
    // CUDA stuff
    float* d_matB;
	cudaError_t status;		// funkcje cuda zwracaja kod bledu jako strukture cudaError_t

    file = fopen(argv[1],"r");
    fscanf(file,"%d",&n);

    matA = new float[n*(n+1)];
    mat_init(matA, n);
    mat_read(file, matA, n);
    fclose(file);

    matB = new float[n*(n+1)];
    mat_init(matB, n);
    memcpy(matB,matA,n*(n+1)*sizeof(float));
	float *a, *b; // macierz dla gaussa jordana
	a= new float[n*n];
	b= new float[n];
	for(int i= 0; i < n; ++i){
		for(int j= 0; j < n; ++j)
			a[i*n+j]= matB[i* (n+1) +j];
		b[i]= matB[ i* (n+1) + n];
	}
    //printf("Input matrix:\n");
    //mat_print(matA, n);

    result = new float[n];
    cudaResult = new float[n];

    vect_init(result,n);
    vect_init(cudaResult,n);
	
	cout<<"############ GAUSS ######################"<<endl;
    printf("************ SECQUENCE STYLE ************\n");
    start = clock();
    seqGauss(n,matA,result);
    end = clock();
    //mat_print(matA, n);
    getResult(n,matA,result);
    vect_print(result,n);
    printf("Time: %lf s\n", (end - start) / (double)CLOCKS_PER_SEC);

    
    //** CUDA Panie, CUDA **//
    /***********************/
    
	// alokacja pamieci na karcie
	status= cudaMalloc( (void**) &(d_matB), sizeof(float)*n*(n+1));
	if( status != cudaSuccess)
		cudaErr("Error: cudaMalloc for d_matB: ", status);

	// kopiowanie z hosta na karte
	status= cudaMemcpy( d_matB, matB, sizeof(float)*n*(n+1), cudaMemcpyHostToDevice);
	if( status != cudaSuccess )
		cudaErr("Error: memCpy Host->Device: ", status);

    // podzil pracy
    int blocks;
    int threads= th;
	blocks= n / threads;
	if( (n % threads) != 0)
		blocks++;

    // obliczenia na karcie
    printf("/************ CUDA STYLE ************\n");
    start = clock();
    for(int i = 0; i < n - 1; i++)
        gauss<<< blocks, threads>>>(d_matB, n, i);
    end = clock();

    // kopiowanie z karty na hosta 
	status= cudaMemcpy(matB, d_matB, sizeof(float) * n * (n+1), cudaMemcpyDeviceToHost);
	if( status != cudaSuccess)
		cudaErr("Error: memCpy Device -> Host: ", status);

    //mat_print(matB,n);
    getResult(n,matB,cudaResult);
    vect_print(cudaResult,n);
    printf("Time: %lf s\n", (end - start) / (double)CLOCKS_PER_SEC);

    //** Koniec CUDA Panie, koniec **//
    /********************************/

    // clean up
    mat_delete(matA, n);
    mat_delete(matB, n);
    delete[] result;
    delete[] cudaResult;
    cudaFree(d_matB);

	cout<<"############ GAUSS JORDAN ################"<<endl;
	float *x, *sq_x;
	sq_x= new float[n];
	
	
	
	cout<<"************ SECQUENCE STYLE ************"<<endl;
    start = clock();
	seqGJ(a, b, sq_x, n);
    printf("Time: %lf s\n", (end - start) / (double)CLOCKS_PER_SEC);
	vect_print(sq_x, n);
	cout<<"************ CUDA STYLE *****************"<<endl;
    start = clock();
	x= GJelimination<float>(a, b, n);
    printf("Time: %lf s\n", (end - start) / (double)CLOCKS_PER_SEC);
	vect_print(x,n);
	
	delete[] a;
	delete[] b;
	delete[] sq_x;
	delete[] x;
	
    return 0;
}
